﻿using SmokeBasin.Models;
using System;
using System.Collections.Generic;

namespace SmokeBasin
{
    class Program
    {
        static void Main(string[] args)
        {
            Map map;

            try
            {
                // "Files\heightmap1.txt"
                // "Files\heightmap1_incorrect.txt"
                // "Files\heightmap2.txt"
                map = new Map(@"Files\heightmap2.txt");
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }

            List<Point> lowestPoints = map.GetLowestPoints();

            int sum = 0;

            foreach (var point in lowestPoints)
            {
                sum += point.Height + 1;
            }

            int[] orderedBasins = map.GetOrderedBasins();

            Console.WriteLine($"Sum of low points: {sum}");
            Console.WriteLine($"Multiplication of three largest basins: {orderedBasins[0] * orderedBasins[1] * orderedBasins[2]}");
            Console.ReadKey();
        }
    }
}
