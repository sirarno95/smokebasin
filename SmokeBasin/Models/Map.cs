﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SmokeBasin.Models
{
    public class Map
    {
        public int MinimumHeight { get; set; } = int.MaxValue;
        public int MaximumHeight { get; set; } = int.MinValue;
        public Point[,] Points { get; set; }
        public List<List<Point>> Basins { get; set; }

        public Map(string mapPath)
        {
            this.InitMap(mapPath);
            Basins = new List<List<Point>>();
        }

        public List<Point> GetLowestPoints()
        {
            // this.FindLowestPointsByHeight();
            this.FindLowestPointsForLoop();

            List<Point> points = new List<Point>();

            for (int i = 0; i < this.Points.GetLength(0); i++)
            {
                for (int j = 0; j < this.Points.GetLength(1); j++)
                {
                    if (this.Points[i, j].IsTheLowest)
                    {
                        points.Add(this.Points[i, j]);
                    }
                }
            }

            return points;
        }

        public int[] GetOrderedBasins()
        {
            List<Point> lowestPoints = this.GetLowestPoints();

            foreach (var point in lowestPoints)
            {
                List<Point> basin = new List<Point>();
                this.Basins.Add(this.GetBasinByLowestPoint(point, basin));
            }

            return this.Basins.OrderByDescending(x => x.Count).Select(x => x.Count).ToArray();
        }

        private void InitMap(string mapPath)
        {
            string[] lines = File.ReadAllLines(mapPath);
            int row = lines.Length;
            int cell = lines[0].Length;
            this.Points = new Point[row, cell];

            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < cell; j++)
                {
                    int height;
                    if (!int.TryParse(lines[i][j].ToString(), out height))
                    {
                        throw new ArgumentException("Incorrect input file");
                    }

                    Points[i, j] = new Point(i, j, height);

                    if (height > this.MaximumHeight)
                        this.MaximumHeight = height;

                    if (height < this.MinimumHeight)
                        this.MinimumHeight = height;
                }
            }
        }

        private void FindLowestPointsByHeight()
        {
            for (int i = this.MinimumHeight; i < this.MaximumHeight - this.MinimumHeight + 1; i++)
            {
                var points = this.GetMatchingPoints(i);

                foreach (var point in points)
                {
                    if (this.IsTheLowestPoint(point))
                    {
                        point.IsTheLowest = true;
                    }
                }
            }
        }

        private void FindLowestPointsForLoop()
        {
            foreach (var point in this.Points)
            {
                if (this.IsTheLowestPoint(point))
                {
                    point.IsTheLowest = true;
                }
            }
        }

        private List<Point> GetMatchingPoints(int heigth)
        {
            List<Point> points = new List<Point>();

            for (int i = 0; i < this.Points.GetLength(0); i++)
            {
                for (int j = 0; j < this.Points.GetLength(1); j++)
                {
                    if (this.Points[i, j].Height == heigth)
                    {
                        points.Add(this.Points[i, j]);
                    }
                }
            }

            return points;
        }

        private bool IsTheLowestPoint(Point point)
        {
            if (point.Height == this.MinimumHeight)
            {
                return true;
            }
            else if (point.Height == this.MaximumHeight)
            {
                return false;
            }

            // top
            if (this.WithinGrid(point.Row - 1, point.Column) && this.Points[point.Row - 1, point.Column].Height < point.Height)
            {
                return false;
            }
            //down
            if (this.WithinGrid(point.Row + 1, point.Column) && this.Points[point.Row + 1, point.Column].Height < point.Height)
            {
                return false;
            }
            //left
            if (this.WithinGrid(point.Row, point.Column - 1) && this.Points[point.Row, point.Column - 1].Height < point.Height)
            {
                return false;
            }
            //right
            if (this.WithinGrid(point.Row, point.Column + 1) && this.Points[point.Row, point.Column + 1].Height < point.Height)
            {
                return false;
            }

            return true;
        }

        private List<Point> GetBasinByLowestPoint(Point point, List<Point> basin)
        {
            basin.Add(point);

            // get neighbour
            var neighbours = this.GetNeighbourPoints(point);

            foreach (var neighbour in neighbours)
            {
                if (neighbour.Height != 9 && !basin.Any(x => x.Row == neighbour.Row && x.Column == neighbour.Column))
                {
                    GetBasinByLowestPoint(neighbour, basin);
                }
            }

            return basin;
        }

        private List<Point> GetNeighbourPoints(Point point)
        {
            List<Point> neighbours = new List<Point>();

            if (WithinGrid(point.Row - 1, point.Column))
                neighbours.Add(this.Points[point.Row - 1, point.Column]);

            if (WithinGrid(point.Row + 1, point.Column))
                neighbours.Add(this.Points[point.Row + 1, point.Column]);

            if (WithinGrid(point.Row, point.Column - 1))
                neighbours.Add(this.Points[point.Row, point.Column - 1]);

            if (WithinGrid(point.Row, point.Column + 1))
                neighbours.Add(this.Points[point.Row, point.Column + 1]);

            return neighbours;
        }

        private bool WithinGrid(int rowNum, int colNum)
        {
            if ((rowNum < 0) || (colNum < 0))
            {
                return false;
            }
            if ((rowNum >= Points.GetLength(0)) || (colNum >= Points.GetLength(1)))
            {
                return false;
            }
            return true;
        }
    }
}
