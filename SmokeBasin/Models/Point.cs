﻿namespace SmokeBasin.Models
{
    public class Point
    {
        public int Row { get; set; }
        public int Column { get; set; }
        public int Height { get; set; }
        public bool IsTheLowest { get; set; }

        public Point(int row, int cell, int height, bool isTheLowest = false)
        {
            Row = row;
            Column = cell;
            Height = height;
            IsTheLowest = isTheLowest;
        }
    }
}
